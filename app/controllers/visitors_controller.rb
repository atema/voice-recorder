# frozen_string_literal: true

class VisitorsController < ApplicationController
  def index
    @calls = Call.where(company_id: current_user.company_id).order(created_at: :desc)
  end
end
