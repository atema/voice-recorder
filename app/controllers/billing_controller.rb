# frozen_string_literal: true

class BillingController < ApplicationController
  def index; end

  def view
    @month = params[:date][:month]
    @year = params[:date][:year]
    @first_day = DateTime.new(@year.to_i, @month.to_i, 1, 0, 0, 0, 0)
    @last_day = @first_day.end_of_month

    @calls = Call.where(created_at: @first_day..@last_day)
  end
end
