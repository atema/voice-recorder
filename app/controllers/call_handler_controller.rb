# frozen_string_literal: true

class CallHandlerController < ApplicationController
  layout false
  skip_before_action :authenticate_user!

  def inbound_call
    require 'twilio-ruby'
    # The params passed here should contain the number,
    id_number = [params[:number]].first

    ######## TODO REJECT IF NO NUMBER #######
    # If the number isnt in our DB, reject with an error message
    company = Company.where(number: id_number).first

    ######## TODO REJECT IF NO COMPANY #######
    message = company.message
    # If it is, play the configured message for the company
    # The action for recording should be the call_recording action
    response = Twilio::TwiML::VoiceResponse.new do |r|
      r.say(message: message)
      if Rails.env.development?
        r.record(action: 'http://a84608bc.ngrok.io/call_handler/call_recording', method: 'GET', max_length: 3600, timeout: 20)
      elsif Rails.env.staging?
        r.record(action: 'http://m2m.welcometelecom.co.uk/dev/call_handler/call_recording', method: 'GET', max_length: 3600,  timeout: 20)
      elsif Rails.env.production?
        r.record(action: 'https://m2m.welcometelecom.co.uk/call_handler/call_recording', method: 'GET', max_length: 3600, timeout: 20)
      end
    end
    # &Caller=%2B447824704948
    called_number = [params[:Called]].first
    called_number = called_number.gsub('%B', '')
    called_number = called_number.gsub('+', '')

    from = [params[:From]].first
    from = from.gsub('%B', '')
    from = from.gsub('+', '')
    # TODO: handle if there is no number
    # Find the company associated with this
    company = Company.where(number: called_number).first
    callSID = [params[:CallSid]].first
    call = Call.new(callsid: callSID, company_id: company.id, from: from, status: 'Awaiting Recording')
    call.save
    call.logs.new(message: 'Call Received: Starting Recording').save
    # print the result
    render xml: response.to_s
  end

  def call_recording
    # Retrieve the phone number sent in the post header
    # &Caller=%2B447824704948
    called_number = [params[:Called]].first
    called_number = called_number.gsub('%B', '')
    called_number = called_number.gsub('+', '')
    duration = [params[:RecordingDuration]].first
    duration = duration.gsub('\\"', '')
    duration = duration.gsub('"', '')
    duration = (duration.to_i + 13).to_s
    # TODO: handle if there is no number
    # Find the company associated with this
    company = Company.where(number: called_number).first
    # Create a new call record for the company, i think we'll just store the callSID here
    callSID = [params[:CallSid]].first
    recordingSID = [params[:RecordingSid]].first
    call = Call.where(callSID: callSID).first
    call.logs.new(message: 'End of recording, now processing').save
    call.update(recordingsid: recordingSID, duration: duration, status: 'Recording Available')
    call.save
    call.logs.new(message: 'New Recording Processed: ' + recordingSID).save
    # TODO: Send an e-mail to notify of a new recording
    NewRecording.new_call_email(call).deliver_now
    require 'twilio-ruby'
    response = Twilio::TwiML::VoiceResponse.new do |r|
    end
    # print the result
    render xml: response.to_s
  end

  def request_transcription; end

  def receive_transcription; end

  def resend_email
    record = Call.where(id: params[:call]).first
    record.logs.new(message: 'E-Mail Resent By:' + current_user.email).save
    NewRecording.new_call_email(record).deliver_now
    session[:return_to] ||= request.referer
    redirect_to session.delete(:return_to)
  end

  def delete_single_recording
    record = Call.where(id: params[:call]).first
    record.logs.new(message: 'Recording Deleted By:' + current_user.email).save
    record.status = 'Recording Deleted'
    record.recordingDeleted = DateTime.now
    record.save

    require 'twilio-ruby'
    account_SID = current_user.company.accountSID
    auth_token = current_user.company.auth_token

    @client = Twilio::REST::Client.new(account_SID, auth_token)
    begin
      response = @client.recordings(record.recordingsid).delete
    rescue StandardError
    end
    session[:return_to] ||= request.referer
    redirect_to session.delete(:return_to)
  end
end
