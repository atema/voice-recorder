# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
 $(document).ready ->
  $('table').each ->
    $table = $(this)
    $button = $('<button type=\'button\'>')
    $button.text 'Export to spreadsheet'
    $button.insertBefore $table
    $button.click ->
      csv = $table.table2CSV(delivery: 'value')
      window.location.href = 'data:text/csv;charset=UTF-8,' + encodeURIComponent(csv)
      return
    return
  return
