class NewRecording < ApplicationMailer
# TODO Send an e-mail to notify of a new recording
default :from => "m2m@welcometelecom.co.uk",
        :reply_to => "ipt@welcometelecom.co.uk"

  def new_call_email(call)
    @call = call
    call.company.users.each do |user|
@user = user

    response =   mail(to: user.email, subject: 'New Voice Recording Received ' +call.created_at.to_s)
      @call.logs.new(:message => "Passed to SMTP Server").save
      @call.logs.new(:message => response.to_s).save
    end
  end
end
