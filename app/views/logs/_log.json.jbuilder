json.extract! log, :id, :call_id, :message, :created_at, :updated_at
json.url log_url(log, format: :json)
