json.extract! call, :id, :callsid, :recordingsid, :created_at, :updated_at
json.url call_url(call, format: :json)
