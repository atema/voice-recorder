json.extract! phonebook, :id, :company_id, :number, :name, :created_at, :updated_at
json.url phonebook_url(phonebook, format: :json)
