# frozen_string_literal: true

# == Schema Information
#
# Table name: calls
#
#  id               :integer          not null, primary key
#  callsid          :string(255)
#  recordingsid     :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  company_id       :integer
#  from             :string(255)
#  duration         :string(255)
#  status           :string(255)
#  recordingDeleted :datetime
#

class Call < ActiveRecord::Base
  belongs_to :company
  has_many :logs, dependent: :destroy

  def isbillable; end
end
