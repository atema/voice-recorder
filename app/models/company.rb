# frozen_string_literal: true

# == Schema Information
#
# Table name: companies
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  email      :string(255)
#  message    :string(255)
#  number     :string(255)
#  accountSID :string(255)
#  auth_token :string(255)
#

class Company < ActiveRecord::Base
  has_many :users,  dependent: :destroy
  has_many :calls,  dependent: :destroy
  has_many :phonebooks, dependent: :destroy
end
