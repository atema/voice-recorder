# frozen_string_literal: true

class Phonebook < ActiveRecord::Base
  belongs_to :company
end
