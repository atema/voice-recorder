# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190703131032) do

  create_table "calls", force: :cascade do |t|
    t.string   "callsid",          limit: 255
    t.string   "recordingsid",     limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "company_id",       limit: 4
    t.string   "from",             limit: 255
    t.string   "duration",         limit: 255
    t.string   "status",           limit: 255
    t.datetime "recordingDeleted"
  end

  add_index "calls", ["company_id"], name: "index_calls_on_company_id", using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "email",      limit: 255
    t.string   "message",    limit: 255
    t.string   "number",     limit: 255
    t.string   "accountSID", limit: 255
    t.string   "auth_token", limit: 255
  end

  create_table "logs", force: :cascade do |t|
    t.integer  "call_id",    limit: 4
    t.text     "message",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "logs", ["call_id"], name: "index_logs_on_call_id", using: :btree

  create_table "phonebooks", force: :cascade do |t|
    t.integer  "company_id", limit: 4
    t.string   "number",     limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "phonebooks", ["company_id"], name: "index_phonebooks_on_company_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "name",                   limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.integer  "company_id",             limit: 4
    t.boolean  "admin"
  end

  add_index "users", ["company_id"], name: "index_users_on_company_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "calls", "companies"
  add_foreign_key "logs", "calls"
  add_foreign_key "phonebooks", "companies"
  add_foreign_key "users", "companies"
end
