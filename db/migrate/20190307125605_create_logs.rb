class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.references :call, index: true, foreign_key: true
      t.text :message

      t.timestamps null: false
    end
  end
end
