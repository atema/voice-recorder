class AddRecordingDeletedToCall < ActiveRecord::Migration
  def change
    add_column :calls, :recordingDeleted, :datetime
  end
end
