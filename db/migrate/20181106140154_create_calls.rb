class CreateCalls < ActiveRecord::Migration
  def change
    create_table :calls do |t|
      t.string :callsid
      t.string :recordingsid

      t.timestamps null: false
    end
  end
end
