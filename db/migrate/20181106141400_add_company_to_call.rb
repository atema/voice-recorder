class AddCompanyToCall < ActiveRecord::Migration
  def change
    add_reference :calls, :company, index: true, foreign_key: true
  end
end
