Rails.application.routes.draw do
  resources :phonebooks
  get 'billing/index'

  get 'billing/view'

  get 'billing/index'

  resources :logs
  resources :calls
  mount RailsAdmin::Engine => '/atema', as: 'rails_admin'
  resources :companies
  root to: 'visitors#index'
  devise_for :users, :skip => [:registrations]
  as :user do
  get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
  put 'users' => 'devise/registrations#update', :as => 'user_registration'

  # Call Handling Routes
  get 'call_handler/inbound_call' => 'call_handler#inbound_call'
  get 'call_handler/call_recording' => 'call_handler#call_recording'
  get 'call_handler/resend_email' => 'call_handler#resend_email', as: :resend_email
  get 'call_handler/delete_single_recording' => 'call_handler#delete_single_recording', as: :delete_single_recording

end
  resources :users
end
